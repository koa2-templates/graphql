module.exports = {
  app: {
    name: 'koa2-scaffold',
    env: 'base',
  },

  http: {
    port: 1226,
  },

  logger: {
    console: {
      enable: true,
      transport: 'Console',
      option: {
        json: true,
        stringify: true,
        timestamp: true,
        logstash: true,
        humanReadableUnhandledException: true,
      },
    },
  },

  mongo: {
    testDB: {
      hosts: ['localhost'],
      database: 'test',
      options: {
        db: {
          readPreference: 'secondaryPreferred',
        },
      },
    },
  },
};
