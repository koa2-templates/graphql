const Koa = require('koa');
const http = require('http');
const config = require('config');
const koaWinston = require('koa2-winston');
const bodyParser = require('koa-bodyparser');

const db = require('./src/models');
const routes = require('./src/routes');
const logger = require('./src/components/logger');
const response = require('./src/middlewares/response');
const errorHandler = require('./src/middlewares/error_handler');

const app = new Koa();

app.context.db = db;
// error handler
app.use(errorHandler());

app.use(bodyParser());
app.use(koaWinston.logger());

app.use(response());
app.use(routes());

const server = http.createServer(app.callback());

if (!module.parent) {
  server.listen(config.http.port || 1226);
  server.on('listening', () => {
    logger.info('Server is listening on port:%d', server.address().port);
  });
}

module.exports = server;
