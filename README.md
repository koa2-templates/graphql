
Koa2-scaffold is a template for koa2 app with common components, such as logger,
error handler, test ,CI and so on.

## router
[koa-router](https://github.com/alexmingoia/koa-router)

## logger
winston

## validation
koa2-validation

## test
ava nyc supertest  nock
