const RootQuery = `
  type Query {
    hello: String
    post(id: String): Post
    posts: [Post]
    author(id: String): Author
    authors: [Author],
  }
`;

module.exports = RootQuery;
