module.exports = `
  type Author {
    id: String
    name: String
    gender: String
    age: Int
    posts: [Post]
  }
`;
