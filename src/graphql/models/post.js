module.exports = `
  type Post {
    id: String
    title: String
    author: Author
    votes: Int
  }
`;
