const { makeExecutableSchema } = require('graphql-tools');

const models = require('./models');
const resolvers = require('./resolvers');
const query = require('./query');

const base = `
  schema {
    query: Query
  }
`;
const typeDefs = [base, query, ...models];

const schema = makeExecutableSchema({ typeDefs, resolvers });

module.exports = schema;
