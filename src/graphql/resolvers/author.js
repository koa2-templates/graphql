const getAuthorList = async (obj, args, context) => {
  const { db } = context;
  const authors = await db.author.find();
  return authors;
};

const getPostList = async (obj, args, context) => {
  const { db } = context;
  const posts = await db.post.find({ author: obj._id.toString() });
  return posts;
};

const getAuthorInfo = async (obj, args, context) => {
  const { id } = args;
  const { db } = context;
  const author = await db.author.findById(id);
  return author;
};
module.exports = {
  Query: {
    authors: getAuthorList,
    author: getAuthorInfo,
  },
  Author: {
    posts: getPostList,
  },
};
