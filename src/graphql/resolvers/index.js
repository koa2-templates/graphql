const _ = require('lodash');

const post = require('./post');
const author = require('./author');

const base = {
  Query: {
    hello: () => 'dennis',
  },
};

module.exports = _.merge(base, post, author);
