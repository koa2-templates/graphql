const getPostList = async (obj, args, context) => {
  const { db } = context;
  const posts = await db.post.find();
  return posts;
};

const getAuthor = async (obj, args, context) => {
  const { db } = context;
  const author = await db.author.findById(obj.author);
  return author;
};

const getPostInfo = async (obj, args, context) => {
  const { id } = args;
  const { db } = context;
  const post = await db.post.findById(id);
  return post;
};
module.exports = {
  Query: {
    posts: getPostList,
    post: getPostInfo,
  },
  Post: {
    author: getAuthor,
  },
};
