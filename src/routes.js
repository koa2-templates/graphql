const config = require('config');
const router = require('koa-router')();
const validate = require('koa2-validation');
const { graphqlKoa, graphiqlKoa } = require('apollo-server-koa');

const schema = require('./graphql/schema');
const user = require('./controllers/user');

module.exports = () => {
  // health check
  router.get('/check', ctx => (ctx.body = `${config.app.name} is working`));

  // graphql
  router.post('/graphql', graphqlKoa(ctx => ({ schema, context: { db: ctx.db } })));
  router.get('/graphql', graphqlKoa(ctx => ({ schema, context: { db: ctx.db } })));
  router.get('/graphiql', graphiqlKoa({ endpointURL: '/graphql' }));

  router.post('/users', validate(user.v.addUser), user.addUser);
  router.get('/users/:id', validate(user.v.getUserInfo), user.getUserInfo);
  router.get('/users', validate(user.v.getUserList), user.getUserList);

  return router.routes();
};
