const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const db = require('../components/mongodb').testDB;

const AuthorSchema = new Schema({
  name: String,
  age: Number,
  gender: {
    type: String,
    enum: ['female', 'male'],
  },
}, {
  timestamps: { createdAt: 'createtime', updatedAt: 'updated_at' },
});

module.exports = db.model('Author', AuthorSchema, 'author');
