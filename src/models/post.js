const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const db = require('../components/mongodb').testDB;

const PostSchema = new Schema({
  title: String,
  author: {
    type: String,
    ref: 'Author',
  },
  votes: Number,
}, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = db.model('Post', PostSchema, 'post');
