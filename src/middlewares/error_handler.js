/**
 * use async await to catch app error
 * @param {Object} [app] - koa app instance
 * @param {Object} [payload] - config for this middleware
 * @param {boolean} payload.sentry - whether enable sentry
 */
module.exports = (app, payload = {}) => {
  const { sentry = false } = payload;

  return async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      console.log(err);
      const status = err.status || err.statusCode;
      ctx.status = status || 500;
      ctx.body = {
        success: false,
        code: err.code,
        message: err.message,
      };

      // once sentry enable
      // should send error by cilent
      if (sentry && app.sentry !== undefined) {
        app.sentry.captureException(err);
      }
    }
  };
};
