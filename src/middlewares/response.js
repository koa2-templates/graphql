module.exports = (payload = {}) => {
  const { status = 200 } = payload;

  return async (ctx, next) => {
    ctx.reply = (data, code) => {
      ctx.status = code || status;
      ctx.body = { success: true, data };
    };

    ctx.replyPage = (data, code) => {
      ctx.status = code || status;
      const result = { success: true };
      ctx.body = Object.assign({}, result, data);
    };

    await next();
  };
};
