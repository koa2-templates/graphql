const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const config = require('config').mongo;
const logger = require('./logger');

const createDb = (payload) => {
  const uri = payload.uri || `mongodb://${payload.hosts.join(',')}/${payload.database}`;
  logger.info(`mongoose connection uri: ${uri}`);

  const db = mongoose.createConnection(uri, payload.options);
  const retry = (err) => {
    logger.error(`mongoose connection error: ${err}, retry after 5s`);
    return setTimeout(() => db.open(uri), 5000);
  };
  db.on('connected', () => logger.info('mongoose connected'));
  db.on('error', retry);
  db.on('disconnect', retry.bind(retry, 'disconnect'));

  return db;
};

const dbs = new Proxy(
  {},
  {
    get(target, dbName) {
      if (dbName in target) {
        return target[dbName];
      }
      if (typeof dbName !== 'string') {
        return undefined;
      }
      const validDBName = dbName.endsWith('DB') && dbName in config;
      if (!validDBName) {
        return logger.error(`invalid ${dbName} to get db`);
      }

      const payload = config[dbName];
      payload.options = Object.assign({}, config.options, payload.options);
      target[dbName] = createDb(payload);
      return target[dbName];
    },
  });

module.exports = dbs;
