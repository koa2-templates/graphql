const _ = require('lodash');
const winston = require('winston');
const config = require('config').logger;

const transports = _.map(config, (payload) => {
  if (!payload.enable) { return 0; }

  return new (winston.transports[payload.transport])(payload.option);
}).filter(_.identity);

const logger = new (winston.Logger)({ transports });

module.exports = logger;
