const _ = require('lodash');
const Joi = require('joi');

const domain = require('../models');

const v = {};
exports.v = v;

v.addUser = {
  body: {
    name: Joi.string(),
    age: Joi.number(),
  },
};
exports.addUser = async (ctx) => {
  const user = ctx.request.body;

  const u = await domain.user.create(user);

  ctx.reply(u);
};

v.getUserInfo = {
  params: {
    id: Joi.string().regex(/[0-9a-z]{24}/).required(),
  },
};
exports.getUserInfo = async (ctx) => {
  const { id } = ctx.params;
  const user = await domain.user.findById(id);

  ctx.reply(user);
};

v.getUserList = {
  query: {
    page: Joi.number().default(1),
    size: Joi.number().default(10),
  },
};
exports.getUserList = async (ctx) => {
  const { page, size } = ctx.query;

  const total = await domain.user.count({});
  const list = await domain.user.find({})
    .skip((page - 1) * size)
    .limit(size)
    .exec();

  ctx.replyPage({ total, page, size, data: list });
};

