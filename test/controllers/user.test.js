const test = require('ava').test;
const helper = require('../helper');

const urls = {
  list: '/users',
};

test.beforeEach(() => {
  const module = ['user'];
  return helper.dropDb()
    .then(helper.initDb.bind(null, module));
});

test('user: should return user list', async (t) => {
  const { statusCode, body } = await helper.request({ url: urls.list });

  t.is(statusCode, 200);
  t.is(body.success, true);
});

