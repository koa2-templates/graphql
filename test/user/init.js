const Promise = require('bluebird');

const model = require('../../src/models');

exports.data = [
  {
    name: 'test',
    age: 18,
    gender: 'male',
  },
];

exports.initData = () =>
  Promise.all(exports.data.map(u => model.user.create(u)));

exports.drop = () => model.user.remove({});
